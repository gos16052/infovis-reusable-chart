function ScatterPlot(){
    var width = 500, 
        height = 500, 
        margin = { left:50, right:50, top:50, bottom:50 };

    var unique = function(x){return x.reverse().filter(function (e, i, x) {return x.indexOf(e, i+1) === -1;}).reverse();}

    // TODO: Complete Initialization Below ////////////////////////////////////////////
    var xVal = function(d){ return d[0];},
        xScale = d3.scaleLinear(), 
        xAxis  = d3.axisBottom(xVal) ;

    var yVal = function(d){return d[1];}, 
        yScale = d3.scaleLinear(),
        yAxis = d3.axisLeft(yVal);
    
    var colorVal = function(d){return d[2];},
        colorScale = d3.scaleOrdinal(),
        colorScheme = d3.schemeAccent;

    var shapeVal = function(d){return d[3];},
        shapeScale = d3.scaleOrdinal(),
        shapeScheme = d3.symbols;

    var sizeVal = function(d){return d[4];},
        sizeScale = d3.scaleLinear(),
        sizeScheme = [8., 15.];
    
    
    var visualVars = {};


    //////////////////////////////////////////////////////////////////////////////////
    
    function translate(xDelta, yDelta){
        return "translate(" + xDelta + "," + yDelta + ")"
    }

    var my = function (selection){
        selection.each(function(data){
            
            var ucolor = unique(data.map(function(d,i) { return colorVal.call(data,d,i)}))
            var ushape = unique(data.map(function(d,i) { return shapeVal.call(data,d,i)}))

            data = data.map(function(d,i) {
                return [
                    xVal.call(data,d,i), 
                    yVal.call(data,d,i),
                    colorVal.call(data,d,i),
                    shapeVal.call(data,d,i),
                    sizeVal.call(data,d,i)
                ];
            })
            // TODO: Complete Scaling Below //////////////////////////////////////////
            xScale
                .domain(d3.extent(data, function(d){return d[0];})).nice()
                .range([margin.left, width - margin.right - margin.left]);
            
            yScale
                .domain(d3.extent(data, function(d){return d[1];})).nice()
                .range([height - margin.top, margin.top]);

            colorScale
                .domain(ucolor)
                .range(colorScheme)

            
            shapeScale
                .domain(ushape)
                .range(shapeScheme)
            
            sizeScale
                .domain(d3.extent(data, function(d){return d[4];})).nice()
                .range([8., 15.])
                
            //////////////////////////////////////////////////////////////////////////
            
            var svg = d3.select(this)
                        .append("svg")
                        .attr("width", width)
                        .attr("height", height)
                        .append("g")
                        .attr("transform", translate(margin.left, 0));

            
            
            // TODO: Render X-Axis Below /////////////////////////////////////////////
            xAxis
                .scale(xScale)
                .tickFormat(d3.formatPrefix(".1", 1e6))
                .ticks(10)
            
            svg
                .append("g")
                .attr("class", "x-axis")
                .attr("transform", translate(margin.left,height-margin.top))
                .call(xAxis);
            

            ///////////////////////////////////////////////////////////////////////////
            
            // TODO: Render Y-Axis Below //////////////////////////////////////////////
            yAxis.scale(yScale)
                .tickFormat(d3.formatPrefix(".1", 1e6))
                .ticks(10)

            svg
                .append("g")
                .attr("class", "y-axis")
                .attr("transform", translate(margin.left,0))
                .call(yAxis);

            ///////////////////////////////////////////////////////////////////////////

            // TODO: Render Points Below //////////////////////////////////////////////
            svg
                .selectAll(".point")
                .data(data)
                .enter()
                .append('path')
                .attr('class', 'point')
                .attr("d", d3.symbol()
                        .size(function(d) { return Size(d)*10; })
                        .type(function(d) { return Shape(d);} )
                )
                .style("fill", function(d) { return Color(d); })
                .attr("transform", function(d){
                        return translate(X(d), Y(d));
                })
               

               
            ///////////////////////////////////////////////////////////////////////////
            
            brushUpdate = function(){
                var extent = d3.event.selection;
                var widthSum=0, heightSum=0, n=0;
                var widthRange = [extent[0][0], extent[1][0]];
                var heightRange = [extent[0][1], extent[1][1]];
                var freq_color = new Array(ucolor.length).fill(0);
                var freq_shape = new Array(ushape.length).fill(0);
                
                svg
                    .selectAll('.point')
                    .style('opacity', 0.3)
                    .filter(function(d){
                        return widthRange[0] <= X(d) && 
                            X(d) <= widthRange[1] &&
                            heightRange[0] <= Y(d) &&
                            Y(d) <= heightRange[1];
                    })
                    .style('opacity', 1)
                    .each(function(d){
                        n++;
                        widthSum += d[0];
                        heightSum += d[1];
                        freq_color[ucolor.indexOf(d[2])]++;
                        freq_shape[ushape.indexOf(d[3])]++;
                    })
                d3.select("#num-of-points").text(n);
                d3.select("#freq-color").text();
                d3.select("#freq-shape").text();
                if(n > 0){
                    d3.select("#mean-x").text(d3.format('.2f')(widthSum/n/1000000)+"M"); //-margin.left+"M"
                    d3.select("#mean-y").text(d3.format('.2f')(heightSum/n/1000000)+"M");
                    var colortext = "", shapetext = ""
                    for(i=0;i<ucolor.length;i++)
                        colortext += ucolor[i] + ":" + freq_color[i] + "  ";
                    for(i=0;i<ushape.length;i++)
                        shapetext += ushape[i] + ":" + freq_shape[i] + "  ";
                    d3.select("#freq-color").text(colortext);
                    d3.select("#freq-shape").text(shapetext);
                } else {
                    d3.select("#mean-x").text("");
                    d3.select("#mean-y").text("");
                    d3.select("#freq-color").text("");
                    d3.select("#freq-shape").text("");
                }
            }
            var brush = d3.brush()
                .extent([[margin.left,margin.top],[width+margin.left,height-margin.top]])
                .on('brush', brushUpdate)

            svg
                .append("g")
                .attr("class","brush")
                .call(brush)
            
        });
    }


    // TODO: Complete Getters and Setters /////////////////////////////////////////////
    my.width = function(value){
        if(!arguments.length) return width;
        width = value;
        return my;
    }

    my.visualVariables = function(param){
        if(param==undefined) return visualVars
        visualVars = param;
        return my
    }

    function X(d){
        return xScale(d[0]);
    }
    
    function Y(d){
        return yScale(d[1]);
    }

    function Color(d){
        return colorScale(d[2]);
    }

    function Shape(d){
        return shapeScale(d[3]);
    }

    function Size(d){
        return sizeScale(d[4]);
    }


    my.x = function(val){
        if(val==undefined) return xVal;
        xVal = val;
        return my
    }

    my.y = function(val){
        if(val==undefined) return yVal;
        yVal = val;
        return my
    }

    my.color = function(val){
        if(val==undefined) return colorVal;
        colorVal = val
        return my
    }

    my.shape = function(val){
        if(val==undefined) return shapeVal;
        shapeVal = val;
        return my;
    }

    my.size = function(val){
        if(val==undefined) return sizeVal;
        sizeVal = val;
        return my;
    }

    my.height = function(val){
        if(val==undefined) return heightVal;
        heightVal = val;
        return my;
    }

    my.margin = function(val){
        if(val==undefined) return margin;
        margin = val;
        return my;
    }
    
    ///////////////////////////////////////////////////////////////////////////////////

    return my;
}




